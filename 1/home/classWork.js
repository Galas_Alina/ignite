var arr = [1, 90, 789, 56, 45, 34, 678, 78, -90, -6, 5, 3, 6];
//task 1

function find (arr, val) {
    return arr.indexOf(val);
}
function find2 (arr, val) {
    var answer;
    for(var i = 0; i<=arr.length; i++) {
        if (arr[i] === val) {
            return i;
        } 
    }
    return -1;
}

//task 2
function sortArr (data) {
    var tmp;
    for (var i = data.length - 1; i > 0; i--) {
        var counter = 0;
        for (var j = 0; j < i; j++) {
            if (data[j] > data[j + 1]) {
                tmp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = tmp;
                counter++;
            }
        }
        if (counter == 0) {
            break;
        }
    }
    return data;
}

// task 3
function bigToSmall (arr) {
var  compareNums = function(a, b) {
    return a-b;
}
   var joinedArr = arr.reduce(function (prew, curr) {
        return prew.concat(curr);
    });
   return joinedArr.sort(compareNums).reverse().join('>');
}
console.log(bigToSmall([[34,0],[1,2],[8,9],[3,4],[5,6]]));