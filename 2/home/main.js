// task 1
function summator () {
    var counter = 0;
    return function (num) {
        return counter =  counter + num;
    }
}

var sum = summator();
// alert(sum(2));
// alert(sum(2));
// alert(sum(2));
// alert(sum(2));


//task 2
function strCount () {
    var counter = 0;
     return (function checkProperty (obj) {
        if (typeof obj == "string") {
            counter++; //если передана строка - возвращаем один
        }
        else if ((typeof obj == "object" || typeof obj == 'function') && obj != undefined ) {
            var keys = Object.keys(obj); // если передан объект - выясняеколичество его свойств
            for (var i = 0; i < keys.length; i++) { //перебераем свойства
                checkProperty (obj[keys[i]]) //для каждого вызываем функцию проверки на строку
            }
            return counter; 
        } 
        return counter; 
    })();
}

var a = {
    first: "1",
  second: "2",
  third: false,
  fourth: ["anytime",2,3,4],
  fifth:  null
  }
 //alert(strCount(a));


// Task 3
function add (n) {
    return function (n2) {
        alert(n + n2);
    }
}
//add(2)(3);