// Task 1
var answer1 = "alert( [] == [] ); // false - Первое равенство возвращает false, по тому, что при сравнении создаются два разных объекта. Так как это ссылочный тип, то равны объекты могут быть только, если это один и тот же объект (либо сравниваются 2 переменные, которые ссылаются на один объект)";
var answer2 = "alert( [] ==! [] ); // true - Восклицательный знак - логический оператор, который преобразовывает объект, находящийся справа к еего логическому эквиваленту и инвертирует его, получается false. Далее, мы приходим к сравнению разных типов (object и boolean), что по умолчанию реализовывается путём численного преобразования. Массив не имеет встроенного метода valueOf, значит сработает toString, и преобразует объект к пустой строке. Получается, мы сравниваем 0 и ''. Опять выходит разный тип и опять численное преобразование. Пустая строка в численном эквиваленте - это 0, выходит 0==0 ";
document.write(answer1 + "<br><br>" + answer2);

//task 2

function Calculator () {
      this.firstNum;
      this.secNum;    
      //var that = this;
      this.read = function() {
            this.firstNum = parseInt(prompt('Введите число 1'));
            this.secNum = parseInt(prompt('Введите число 2'));
      };
      this.sum = function() { 
           return this.firstNum + this.secNum; 
      }
      this.mul = function() { 
           return this.firstNum*this.secNum; 
      }
}

var calculator = new Calculator();
calculator.read();
alert(calculator.mul());
alert(calculator.sum());

//tak 3
function Summator () {
      this.run = function () {
            var firstNum = parseInt(prompt('Введите число 1'));
            var secNum = parseInt(prompt('Введите число 2'));
            this.sum(firstNum, secNum);
      }
      this.sum = function (a,b) {
            alert(a+b);
      }
}
summator = new Summator ();
// summator.run();