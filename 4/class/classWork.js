
//Task 1
function sumArgs (a,b,c,d) {
      return Array.prototype.reduce.call(arguments, function(a,b) {
            return a+b;
      });
}
//alert(sumArgs(1,2))

//Task 2

var user = {
  firstName: "Вася",
  sayHi: function() {
    alert( this.firstName );
  }
}; 
function a() {
};
var f = user.sayHi.bind(user);  
f(); 

// Task 3
function joinArgs (a,b,c,d) {
      arguments.join = Array.prototype.join;
      return arguments.join("*");
}
alert(joinArgs(3,6,5));