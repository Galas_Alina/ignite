//Task 1
Function.prototype.customCall = function (context, argument1, argument2) {
  arguments.slice = [].slice;
  var args = arguments.slice(1);
  this.apply(context, args);
}
function a (a, b) {
  alert(this)
  alert(a+b)
}
//a.customCall(1,2,3);

//Task 2
var doubleTheValue = function(val) { return val * 2; }
var addOneToTheValue = function(val) { return val + 1; }

function compose (param, f1, f2, f3) {
  if(arguments.length <= 1) {
    return param;
  }
  for (var i = 1; i < arguments.length; i++) {
   param = arguments[i].call(compose, param);
  }
  return param;
}

//alert(compose(5, doubleTheValue, addOneToTheValue)); 
 