
//Task 1
function printNumber() {
  var i = 1;
  var timerId = setInterval(function() {
    console.log(i++);
    if (i == 21) clearInterval(timerId);
  }, 100);
}

printNumber();

//Task 2 

var leader = {
  name: "Василий Иванович",
  age: 35
};
// превращение объекта в JSON
var jsonStr = JSON.stringify(leader);
// возвращаемся к объекту
var newLeader = JSON.parse(jsonStr); 

//Task 3


try {
      function typeOfArg (arg) {
            if(arguments.length > 1){
                  var message = "передано больше одного аргумента";
                  throw new Error(message)
            } 
            alert(typeof arg)
            return typeof arg;
      };

      //typeOfArg(5,4)

} catch (e) {
      alert(e.message);
}