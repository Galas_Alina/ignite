
$(document).ready(function(){
   var scroll = function (e) {
         var id = "#" + $(e.target).attr("data-to");
        var mnuH = $('header .top-mnu').height();
        var top = $(id).offset().top -mnuH;
        $('body,html').animate({scrollTop: top}, 1500);        
    }

    var increase = function (e) {
       $(this).find('.hover-img').toggleClass( "big" );
    }

  $('.top-mnu nav').on('click', scroll);
  $('#contact-us').on('click', scroll);
  $("#up").on('click', scroll);
  $('.hover-block').on('mouseenter mouseleave', increase);

  //validation

    var mailPattern = /^[a-z0-9_-]+@[a-z0-9-]+\.[a-z]{2,6}$/i;
    var subPattern = /[A-Za-z0-9]+$/;
    var namePattern = /[A-Za-z]+$/;
    
    var mail = $('#mail');
    var sub = $('#sub');
    var name = $('#name');

    var check = function(elem, pattern){
        var valid = elem.parent().find('.valid');
        if(elem.val() != ''){
            if(elem.val().search(pattern) == 0){
                valid.text('Подходит');
                elem.removeClass('error').addClass('ok');
            } else{
                    valid.text('Не подходит');
                    $('#submit').attr('disabled', true);
                    elem.addClass('ok');
                }
        } else{
                valid.text('Поле не должно быть пустым!');
                elem.addClass('error');
        }
    };
    var checkMail = function () {
        check(mail, mailPattern ) 
    }
    var checkName = function () {
        check(name, namePattern )
    }
    var checkSub = function () {
        check(sub, subPattern )
    }
        mail.on('input', checkMail);
        sub.on('input', checkSub);
        name.on('input', checkName);

        // scrolling animations

var animateNumbers = function (time) {
    return ( function () {
                $('.stst-num').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');
        $({countNum: $this.text()}).animate({
            countNum: countTo
            },
            {
            duration: 3000,
            easing: 'swing',
            step: function() {
                $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
            }
            });
        });
    })();
};
var circleRange = function () {
    if ($('body').scrollTop()+$('header .top-mnu').height()*4 >= $('.skills').offset().top) {
        $('.circle-progress').addClass('animation');
        $(document).off('scroll', circleRange);
    }
}


var countScroll = function () {
       if ($('body').scrollTop()+$('header .top-mnu').height()*4 >= $('.stat').offset().top) {
           animateNumbers(3000);
           $(document).off('scroll', countScroll);
           $(document).on('scroll', circleRange)
       }

   } 
 


//gallery filters
    var chooseCategory = function (e) {
        var elem = e.target;
        var elemClass ="." + $(elem).attr('id'); 
        if (elemClass=='.all') {
            $('.gallery li').css({'display':'block'});
        } else { 
            $('.gallery li').css({'display':'none'});
            $(elemClass).css({'display':'block'});
        }
    }
    
   $(document).on('scroll', countScroll);
   $('.gallery-nav').on('click', chooseCategory);
});



